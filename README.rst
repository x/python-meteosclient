Python bindings to the OpenStack Meteos API
===========================================

This is a client for the OpenStack Meteos API. There's a Python API (the
``meteosclient`` module), and a command-line script (``meteos``). Each
implements the OpenStack Meteos API. You can find documentation for both
Python bindings and CLI in `Docs`_.

Development takes place via the usual OpenStack processes as outlined
in the `developer guide
<http://docs.openstack.org/infra/manual/developers.html>`_.

.. _Docs: http://docs.openstack.org/developer/python-meteosclient/

* License: Apache License, Version 2.0
* `PyPi`_ - package installation
* `Online Documentation`_
* `Launchpad project`_ - release management
* `Blueprints`_ - feature specifications
* `Bugs`_ - issue tracking
* `Source`_
* `Specs`_
* `How to Contribute`_

.. _PyPi: https://pypi.python.org/pypi/python-meteosclient
.. _Online Documentation: http://docs.openstack.org/developer/python-meteosclient
.. _Launchpad project: https://launchpad.net/python-meteosclient
.. _Blueprints: https://blueprints.launchpad.net/python-meteosclient
.. _Bugs: https://bugs.launchpad.net/python-meteosclient
.. _Source: https://git.openstack.org/cgit/openstack/python-meteosclient
.. _How to Contribute: http://docs.openstack.org/infra/manual/developers.html
.. _Specs: http://specs.openstack.org/openstack/meteos-specs/

